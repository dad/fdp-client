import builder from '@/api/builder'
import configs from '@/api/configs'
import info from './info'
import repository from './repository'
import memberships from './memberships'
import shapes from './shapes'
import tokens from './tokens'
import users from './users'

export default {
  builder,
  configs,
  info,
  memberships,
  repository,
  shapes,
  tokens,
  users,
}
